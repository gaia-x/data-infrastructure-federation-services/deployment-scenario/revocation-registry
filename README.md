# Verifiable Credential Revocation Registry

This is the codebase of an implementation of a draft of [Verifiable Credential Status List v2021](https://www.w3.org/TR/vc-status-list/).

The implemenation contains a set of methods allowing to revoke, suspend, re-active, and verify the status of a Verifiable Credential of a user. To verify the revocation/suspense status of a Verifiable Credential, Verifier can send a request to the Registry or alternatively retrieve the StatusList Credential of the associated Issuer from Registry and then locally verify the status of the given Verifiable Credential.

## Rest API Specification
1. Get a StatusEntry

To issue a user Verifiable Credential, the associated Issuer needs to get a StatusEntry, which contains revocation/suspense status of this VC, then insert it into the raw VC before finally signing it. The implementation provide an endpoint for this purpose:

```
POST /api/v1/revocations/statusEntry

{
  "credentialUrl": "http://localhost:8085/api/v1/revocations/credentials",
  "purpose": "revocation" // Can be revocation or suspense
}
```

Once the request is successfully processed, Issuer will receive a credentialStatus which will be inserted into the user Verifiable Credential. One example of such response is shown below:
```
{
  "type": "StatusList2021Entry",
  "id": "http://localhost:8085/api/v1/revocations/credentials/issuer-revocation#0",
  "statusPurpose": "revocation",
  "statusListIndex": "0",
  "statusListCredential": "http://localhost:8085/api/v1/revocations/credentials/issuer-revocation"
}
```
Note that the credentialUrl is concatenated with IssuerID as well as the purpose to get the statusListCredential in the response. This url is where the Issuer Status List Credential is stored. 

2. Suspend Verifiable Credential

To suspend a user Verifiable Credential, the associated Issuer can send a request to Registry, who can then suspend the user VC and update the Status List Credential of the Issuer.

```
POST /api/v1/revocations/suspend

{
  "credentialStatus": {
    "id": "string",
    "statusPurpose": "string",
    "statusListIndex": "string",
    "statusListCredential": "string",
    "type": "string"
  }
}
```

3. Reactivate Verifiable Credential

If a user Verifiable Credential has been previously suspended, the associated Issuer can send a request to Registry to re-activate it. The Registry updates the Status List Credential of the Issuer. 
```
POST /api/v1/revocations/reactivate

{
  "credentialStatus": {
    "id": "string",
    "statusPurpose": "string",
    "statusListIndex": "string",
    "statusListCredential": "string",
    "type": "string"
  }
}
```

4. Revoke a Verifiable Credential

To revoke a user Verifiable Credential, the associated Issuer can send a request to Registry which is in charge of updating the Status List Credential of the Issuer:
```
POST /api/v1/revocations/revoke

{
  "credentialStatus": {
    "id": "string",
    "statusPurpose": "string",
    "statusListIndex": "string",
    "statusListCredential": "string",
    "type": "string"
  }
}
```

5. Check the status of a Verifiable Credential

To check the status of a user Verifiable Credential, anyone can send a request to Registry to check:

```
POST /api/v1/revocations/verify

{
  "credentialStatus": {
    "id": "string",
    "statusPurpose": "string",
    "statusListIndex": "string",
    "statusListCredential": "string",
    "type": "string"
  }
}
```

6. Status List Credentials Retrieval

If Verifier prefers locally verifying the status of a user Verifiable Credential, Verifier can retrieve the Status List Credential of the associated Issuer from one of the endpoints below:

```
GET /api/v1/revocations/credentials 
GET /api/v1/revocations/credentials/{issuerId}
GET /api/v1/revocations/credentials/{credentialName}
```


## Build and Test Procedure
To test the project, you can clone and run it in two different modes: 

### Run the project with Maven
This option is mostly useful when you want to fork and add more functions in the project. Make sure that you already have at least the version 3.8 of Maven installed on your machine. The project is using a forked version of SSIKit whose jar artifact is published to a private Maven registry (bytesafe). You would need some Maven configuration to have access to this artifact.
1. Config Maven setting.xml

Copy the content of the setting.xml file, which contains bytesafe access token, and put it into the setting.xml in the Maven installation repository of your machine.
2. Compile and Launche the project

To compile and run the project, you execute the following command:

```
mvn clean spring-boot:run
```
Once the container is up and running, you can use the following url to access to the Swagger interface:

```
http://localhost:8085/swagger-ui/index.html
```
### Run the project with Docker
This option is useful to run the project without additional Maven-related configuration. Make sure that you have Docker installed on your machine. Firstly, you build the image with the following commmand:

```
docker build -t registry .
```

The second command is to launch the Registry container. Do not forget to use the -v option to persist all generated Status List Credentials in a volume.

```
docker run -it -p 8085:8085 -v registry:/app/data registry
```
Once the container is up and running, you can use the following url to access to the Swagger interface:

```
http://localhost:8085/swagger-ui/index.html
```

FROM maven:3.8.5-openjdk-17 as build

WORKDIR /opt/app

COPY pom.xml .
COPY settings.xml /usr/share/maven/ref/
COPY ./src ./src
RUN mvn clean install -s /usr/share/maven/ref/settings.xml

#FROM eclipse-temurin:17-jdk-alpine
FROM maven:3.8.5-openjdk-17
VOLUME registry
WORKDIR /app

COPY --from=build /opt/app/target/revocation-registry.jar /app/revocation-registry.jar
COPY ./config ./
ENTRYPOINT ["java", "-jar", "revocation-registry.jar"]
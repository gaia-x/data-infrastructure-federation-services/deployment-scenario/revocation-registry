package eu.gaiax.vc.lifecyclemanagement.exception;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;


@Getter
@Setter
public class ResourceNotFoundException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -7626550788521580992L;

    private String resourceName;
    private String fieldName;
    private String fieldValue;

    public ResourceNotFoundException(String resourceName, String fieldName, String fieldValue){
        super(String.format("%s not found with %s: %s", resourceName, fieldName, fieldValue));
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.resourceName = resourceName;
    }
}

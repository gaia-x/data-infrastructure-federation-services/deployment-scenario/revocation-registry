package eu.gaiax.vc.lifecyclemanagement.service;

import eu.gaiax.vc.lifecyclemanagement.model.PushNotificationRequest;

import java.util.List;

public interface PushNotificationService {
    void sendNotification(PushNotificationRequest request);
    void sendNotificationToTopic(PushNotificationRequest request);
    void subscribeToTopic(List<String> tokens, String topic);
}

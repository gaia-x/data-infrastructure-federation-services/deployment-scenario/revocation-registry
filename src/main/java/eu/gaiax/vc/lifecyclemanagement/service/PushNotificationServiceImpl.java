package eu.gaiax.vc.lifecyclemanagement.service;

import com.google.firebase.messaging.*;
import eu.gaiax.vc.lifecyclemanagement.model.PushNotificationRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@Service
public class PushNotificationServiceImpl implements PushNotificationService {

    private FirebaseMessaging firebaseMessaging;

    public PushNotificationServiceImpl(FirebaseMessaging firebaseMessaging) {
        this.firebaseMessaging = firebaseMessaging;
    }

    @Override
    public void sendNotification(PushNotificationRequest request) {
        Message message = Message.builder()
                .setNotification(Notification.builder()
                        .setTitle(request.getTitle())
                        .setBody(request.getMessage())
                        .build())
                .setToken(request.getToken())
                .build();
        try {
            firebaseMessaging.send(message);
        } catch (FirebaseMessagingException e) {
            log.debug("Failed to send message to {}", request.getToken());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void sendNotificationToTopic(PushNotificationRequest request) {
        Message message = Message.builder()
                .setNotification(Notification.builder()
                        .setBody(request.getMessage())
                        .setTitle(request.getTitle())
                        .build()
                )
                .setTopic(request.getTopic())
                .build();

        try {
            log.info("Sending notification of topic: {}", request.getTopic());
            firebaseMessaging.send(message);
        } catch (FirebaseMessagingException e) {
            log.error("Failed to send notification of topic {}", request.getTopic());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void subscribeToTopic(List<String> tokens, String topic) {

        TopicManagementResponse response = null;
        try {
            response = FirebaseMessaging.getInstance().subscribeToTopic(tokens, topic);
            log.info("{} tokens has been succesfully subscribed to the topic {}", response.getSuccessCount(), topic);
        } catch (FirebaseMessagingException e) {
            log.error("Failed to subscribe clients to topic {}", topic);
            throw new RuntimeException(e);
        }
    }
}
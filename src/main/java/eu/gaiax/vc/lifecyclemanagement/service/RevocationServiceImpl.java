package eu.gaiax.vc.lifecyclemanagement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.gaiax.vc.lifecyclemanagement.exception.ResourceNotFoundException;
import eu.gaiax.vc.lifecyclemanagement.model.StatusList2021Credential;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.model.credential.status.StatusList2021EntryCredentialStatus;
import id.walt.signatory.revocation.*;
import id.walt.signatory.revocation.statuslist2021.StatusList2021EntryClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class RevocationServiceImpl implements RevocationService{

    private StatusListEntryFactory statusListEntryFactory;
    private StatusList2021EntryClientService statusList2021EntryClientService;

    @Value("${vc_root_path}")
    private String vc_root_path;

    public RevocationServiceImpl(StatusListEntryFactory statusListEntryFactory, StatusList2021EntryClientService statusList2021EntryClientService){
        this.statusListEntryFactory = statusListEntryFactory;
        this.statusList2021EntryClientService = statusList2021EntryClientService;
    }

    @Override
    public StatusListRevocationStatus checkRevocation(StatusListRevocationCheckParameter statusListRevocationCheckParameter) {
        StatusListRevocationStatus statusListRevocationStatus = (StatusListRevocationStatus)statusList2021EntryClientService.checkRevocation(statusListRevocationCheckParameter);
        return statusListRevocationStatus;
    }

    @Override
    public void revoke(StatusListRevocationConfig statusListRevocationConfig) {
        statusList2021EntryClientService.revoke(statusListRevocationConfig);
    }

    @Override
    public void reactivate(StatusListRevocationConfig statusListRevocationConfig) {
        statusList2021EntryClientService.reactivate(statusListRevocationConfig);
    }

    @Override
    public StatusList2021EntryCredentialStatus createStatusList2021Entry(StatusListEntryFactoryParameter statusListEntryFactoryParameter) {
        StatusList2021EntryCredentialStatus statusList2021EntryCredentialStatus = statusListEntryFactory.create(statusListEntryFactoryParameter);
        return statusList2021EntryCredentialStatus;
    }

    @Override
    public Map<String, StatusList2021Credential> getAllStatusList2021Credentials() {
        Map<String, StatusList2021Credential> credentials = new HashMap<>();

        List<String> fileNameList = new ArrayList<>();
        try(Stream<Path> stream = Files.walk(Paths.get(vc_root_path))){
            fileNameList = stream.map(Path::normalize)
                    .filter(Files::isRegularFile)
                    .filter(path -> path.getFileName().toString().endsWith(".cred"))
                    .map(path -> path.getFileName().toString())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        fileNameList.forEach(filename -> credentials.put(filename, getStatusList2021Credential(filename)));

        return credentials;
    }
    @Override
    public StatusList2021Credential getStatusList2021Credential(String fileName) {

        Path path = Paths.get(vc_root_path + fileName);
        String vc = null;
        try {
            vc = Files.readString(path);
        } catch (IOException e) {
            log.debug("The vc with the id {} is not found ");
            throw new ResourceNotFoundException("Revocation VC", "File name", fileName);
        }
        StatusList2021Credential statusList2021Credential = new StatusList2021Credential(vc);
        return statusList2021Credential;
    }
}
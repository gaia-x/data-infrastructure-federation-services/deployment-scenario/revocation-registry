package eu.gaiax.vc.lifecyclemanagement.service;

import eu.gaiax.vc.lifecyclemanagement.model.StatusList2021Credential;
import id.walt.credentials.w3c.VerifiableCredential;
import id.walt.model.credential.status.StatusList2021EntryCredentialStatus;
import id.walt.signatory.revocation.*;

import java.util.List;
import java.util.Map;

public interface RevocationService {
    StatusListRevocationStatus checkRevocation(StatusListRevocationCheckParameter statusListRevocationCheckParameter);
    void revoke(StatusListRevocationConfig statusListRevocationConfig);
    void reactivate(StatusListRevocationConfig statusListRevocationConfig);
    StatusList2021EntryCredentialStatus createStatusList2021Entry(StatusListEntryFactoryParameter statusListEntryFactoryParameter);
    Map<String, StatusList2021Credential> getAllStatusList2021Credentials();
    StatusList2021Credential getStatusList2021Credential(String fileName);

}
package eu.gaiax.vc.lifecyclemanagement.enums;

public enum RevocationPurpose {
    SUSPENSION,
    REVOCATION;
}
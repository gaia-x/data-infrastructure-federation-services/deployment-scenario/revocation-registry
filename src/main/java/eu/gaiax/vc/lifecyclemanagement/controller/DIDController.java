package eu.gaiax.vc.lifecyclemanagement.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController
@RequiredArgsConstructor
public class DIDController {

    private final ResourceLoader resourceLoader;
    private final String didDocument = """
            {
                "assertionMethod" : [
                    "did:web:revocation-registry.abc-federation.dev.gaiax.ovh#7bec0064ce44412fbd0731064276c5b6"
                ],
                "authentication" : [
                    "did:web:revocation-registry.abc-federation.dev.gaiax.ovh#7bec0064ce44412fbd0731064276c5b6"
                ],
                "@context" : "https://www.w3.org/ns/did/v1",
                "id" : "did:web:revocation-registry.abc-federation.dev.gaiax.ovh",
                "verificationMethod" : [
                    {
                        "controller" : "did:web:revocation-registry.abc-federation.dev.gaiax.ovh",
                        "id" : "did:web:revocation-registry.abc-federation.dev.gaiax.ovh#7bec0064ce44412fbd0731064276c5b6",
                        "publicKeyJwk" : {
                            "alg" : "EdDSA",
                            "crv" : "Ed25519",
                            "kid" : "7bec0064ce44412fbd0731064276c5b6",
                            "kty" : "OKP",
                            "use" : "sig",
                            "x" : "k1tICfi7QNU9ONKE7XispRacP2KbtTgR-tJB8zn3E6M"
                        },
                        "type" : "Ed25519VerificationKey2019"
                    }
                ]
            }
            """;
    @GetMapping("/.well-known/did.json")
    public String getDIDDocument(){
//        Resource resource = resourceLoader.getResource("classpath:did/did.json");
//        try {
//            File didDocument = resource.getFile();
//            String content = Files.readString(didDocument.toPath());
//            return content;
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
        return didDocument;
    }
}

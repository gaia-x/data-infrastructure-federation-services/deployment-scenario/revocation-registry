package eu.gaiax.vc.lifecyclemanagement.controller;

import eu.gaiax.vc.lifecyclemanagement.model.PushNotificationRequest;
import eu.gaiax.vc.lifecyclemanagement.model.TopicSubscriptionRequest;
import eu.gaiax.vc.lifecyclemanagement.service.PushNotificationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@Tag(
        name = "Push Notification API using FCM Service"
)
@RestController
@RequestMapping("/api/v1/notifications")
public class PushNotificationController {

    private PushNotificationService pushNotificationService;

    public PushNotificationController(PushNotificationService pushNotificationService){
        this.pushNotificationService = pushNotificationService;
    }

    @Operation(
            summary = "Send a notification to a user associated to the provided token"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Succesfully sent the notification to user"
                    )
            }
    )
    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @PostMapping("/token")
    public ResponseEntity sendNotification(@RequestBody PushNotificationRequest request){
        log.debug("Sending a notification to the user with token : {}", request.getToken());
        pushNotificationService.sendNotification(request);
        return ResponseEntity.ok().body("Success");
    }


    @Operation(
            summary = "Send a notification to a list of subscribed clients"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Success"
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "Failed"
                    )
            }
    )
    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @PostMapping("/topic")
    public ResponseEntity sendNotificationToTopic(@RequestBody PushNotificationRequest request){
            log.debug("Sending a notification with the topic: {} ", request.getTopic());
            pushNotificationService.sendNotificationToTopic(request);
            return ResponseEntity.ok().body("Success");

    }

    @Operation(
            summary = "Subscribe a list of tokens to a topic"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Success"
                    ),
                    @ApiResponse(
                            responseCode = "403",
                            description = "Failed"
                    )
            }
    )
    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @PostMapping("/topic/subscribe")
    public ResponseEntity subscribeToTopic(@RequestBody TopicSubscriptionRequest request){
        pushNotificationService.subscribeToTopic(request.getTokens(), request.getTopic());
        return ResponseEntity.ok().body("Success");
    }
}

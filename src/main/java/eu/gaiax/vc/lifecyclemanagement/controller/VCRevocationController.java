package eu.gaiax.vc.lifecyclemanagement.controller;


import eu.gaiax.vc.lifecyclemanagement.enums.RevocationPurpose;
import eu.gaiax.vc.lifecyclemanagement.model.RevocationResponseModel;
import eu.gaiax.vc.lifecyclemanagement.model.StatusList2021Credential;
import eu.gaiax.vc.lifecyclemanagement.service.RevocationService;
import id.walt.model.credential.status.StatusList2021EntryCredentialStatus;
import id.walt.signatory.revocation.StatusListEntryFactoryParameter;
import id.walt.signatory.revocation.StatusListRevocationCheckParameter;
import id.walt.signatory.revocation.StatusListRevocationConfig;
import id.walt.signatory.revocation.StatusListRevocationStatus;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Map;


@Slf4j
@Tag(
        name = "Verifiable Credential Lifecycle Management"
)
@RestController
@RequestMapping("/api/v1/revocations")
public class VCRevocationController {

    private RevocationService revocationService;
    public VCRevocationController(RevocationService revocationService){
        this.revocationService = revocationService;
    }
    @Operation(
            summary = "Get a StatusList2021Entry for a newly generated Verifiable Credential",
            description = "StatusList2021Entry is generated according to the specified purpose"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "201", description = "Successfully created a StatusList2021Entry"),
                    @ApiResponse(responseCode = "401", description = "API Key is needed to make the request"),
                    @ApiResponse(responseCode = "403", description = "Unauthorized Request")
            }
    )
    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @PostMapping("/statusEntry")
    public ResponseEntity<StatusList2021EntryCredentialStatus> getStatusList2021Entry(@RequestParam(defaultValue = "default") String issuerId,
                                                                                      @RequestBody StatusListEntryFactoryParameter statusListEntryFactoryParameter){
        String purpose = statusListEntryFactoryParameter.getPurpose();

        if( !purpose.equals(RevocationPurpose.REVOCATION.name().toLowerCase()) && !purpose.equals(RevocationPurpose.SUSPENSION.name().toLowerCase())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Only revocation and suspense purposes are supported");
        }

        String credentialUrl = statusListEntryFactoryParameter.getCredentialUrl();
        String extendedCredentialUrl = new StringBuilder(credentialUrl)
                .append("/")
                .append(issuerId)
                .append("-")
                .append(purpose)
                .toString();
        StatusList2021EntryCredentialStatus statusList2021EntryCredentialStatus = revocationService.createStatusList2021Entry(new StatusListEntryFactoryParameter(extendedCredentialUrl, statusListEntryFactoryParameter.getPurpose()));
        return new ResponseEntity<>(statusList2021EntryCredentialStatus, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Revoke a Verifiable Credential",
            description = "Revoked a Verifiable Credential by a given RevocationConfig"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Successfully revoked"),
                    @ApiResponse(responseCode = "401", description = "API Key is needed to make the request"),
                    @ApiResponse(responseCode = "403", description = "Unauthorized request")
            }
    )
    @SecurityRequirement(
            name = "X-API-KEY"
    )
    @PostMapping("/revoke")
    public void revokeVerifiableCredential(@RequestBody StatusListRevocationConfig statusListRevocationConfig){
        validateStatusListIndex(statusListRevocationConfig);
        if(! statusListRevocationConfig.getCredentialStatus().getStatusPurpose().equals(RevocationPurpose.REVOCATION.name().toLowerCase())){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Only "+ RevocationPurpose.REVOCATION.name().toLowerCase()+ " is supported" );
        }
        revocationService.revoke(statusListRevocationConfig);
    }

    @Operation(
            summary = "Suspend a Verifiable Credential",
            description = "Suspend a Verifiable Credential by a given RevocationConfig"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Successfully revoked"),
                    @ApiResponse(responseCode = "401", description = "API Key is needed to make the request"),
                    @ApiResponse(responseCode = "403", description = "Unauthorized request")
            }
    )
    @SecurityRequirement(name = "X-API-KEY")
    @PostMapping("/suspend")
    public void suspendVerifiableCredential(@RequestBody StatusListRevocationConfig statusListRevocationConfig){
        validateStatusListIndex(statusListRevocationConfig);
        if(! statusListRevocationConfig.getCredentialStatus().getStatusPurpose().equals(RevocationPurpose.SUSPENSION.name().toLowerCase())){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Only "+ RevocationPurpose.SUSPENSION.name().toLowerCase()+ " is supported" );
        }
        revocationService.revoke(statusListRevocationConfig);
    }

    @Operation(
            summary = "Reactivate a suspended Verifiable Credential",
            description = "Reactivate a suspended Verifiable Credential"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Successfully revoked"),
                    @ApiResponse(responseCode = "401", description = "API Key is needed to make the request"),
                    @ApiResponse(responseCode = "403", description = "Unauthorized request")
            }
    )
    @SecurityRequirement(name = "X-API-KEY")
    @PostMapping("/reactivate")
    public ResponseEntity<Void> reactivateVerifiableCredential(@RequestBody StatusListRevocationConfig statusListRevocationConfig){
        validateStatusListIndex(statusListRevocationConfig);
        if(! statusListRevocationConfig.getCredentialStatus().getStatusPurpose().equals(RevocationPurpose.SUSPENSION.name().toLowerCase())){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Only "+ RevocationPurpose.SUSPENSION.name().toLowerCase()+ " is supported" );
        }
        revocationService.reactivate(statusListRevocationConfig);

        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Check the revocation status of a Verifiable Credential",
            description = "Check the revocation status of a Verifiable Credential"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Verifiable Credential is valid"),
                    @ApiResponse(responseCode = "401", description = "API Key is needed to make the request"),
                    @ApiResponse(responseCode = "403", description = "Unauthorized request")
            }
    )
    @PostMapping("/verify")
    public ResponseEntity<RevocationResponseModel> checkVerifiableCredentialStatus(@RequestBody StatusListRevocationCheckParameter statusListRevocationCheckParameter){

        StatusListRevocationStatus statusListRevocationStatus = revocationService.checkRevocation(statusListRevocationCheckParameter);
        RevocationResponseModel revocationResponseModel = new RevocationResponseModel();
        if(statusListRevocationCheckParameter.getCredentialStatus().getStatusPurpose().equals(RevocationPurpose.REVOCATION.name().toLowerCase())){
            revocationResponseModel.setRevoked(statusListRevocationStatus.isRevoked());
        } else if (statusListRevocationCheckParameter.getCredentialStatus().getStatusPurpose().equals(RevocationPurpose.SUSPENSION.name().toLowerCase())){
            revocationResponseModel.setSuspended(statusListRevocationStatus.isRevoked());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "purpose type not accepted");
        }
        return new ResponseEntity<>(revocationResponseModel, HttpStatus.OK);
    }

    @Operation(
            summary = "Get a StatusList2021 Credential associated to IssuerId and a given Purpose",
            description = "Get a StatusList2021 Credential associated to IssuerId and a given Purpose"
    )

    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful retrieval"
                    )
            }
    )
    @GetMapping("/credentials/issuers/{issuerId}")
    public ResponseEntity<StatusList2021Credential> getStatusList2021Credential(@RequestParam(name = "purpose") RevocationPurpose purpose, @PathVariable("issuerId") String issuerId) throws IOException {
        log.info("Fetching the Statuslist2021Credential of Issuer: {} and Purpose: {}", issuerId);
        StatusList2021Credential credential =  revocationService.getStatusList2021Credential(issuerId + "-" + purpose.name().toLowerCase()+".cred");
        return new ResponseEntity<>(credential, HttpStatus.OK);
    }

    @Operation(
            summary = "Get a StatusList2021 Credential associated to a given name",
            description = "Get a StatusList2021 Credential associated to a given name"
    )

    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Successful retrieval"),
                    @ApiResponse(responseCode = "400", description = "Status List Credential not found")
            }
    )
    @GetMapping("/credentials/{credentialName}")
    public String getStatusList2021Credential(@PathVariable("credentialName") String credentialName) throws IOException {
        log.info("Fetching the Statuslist2021Credential with name: {} ", credentialName);
        StatusList2021Credential credential =  revocationService.getStatusList2021Credential(credentialName + ".cred");
        return credential.getCredential();
    }

    @Operation(
            summary = "Get all StatusList2021 Credentials in the Registry",
            description = "Get all StatusList2021 Credentials in the Registry"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Successful retrieval"),
                    @ApiResponse(responseCode = "400", description = "Failed to fetch Status List Credentials")
            }
    )
    @GetMapping("/credentials")
    public ResponseEntity<Map<String, StatusList2021Credential>> getStatusList2021Credentials(){
        log.info("Get all revocation list credential in the system");
        Map<String, StatusList2021Credential> credentials = revocationService.getAllStatusList2021Credentials();
        return new ResponseEntity<>(credentials, HttpStatus.OK);
    }

    private void validateStatusListIndex(StatusListRevocationConfig statusListRevocationConfig){
        if(Integer.parseInt(statusListRevocationConfig.getCredentialStatus().getStatusListIndex()) == 0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "statusListIndex is not correct");
        }
    }
}
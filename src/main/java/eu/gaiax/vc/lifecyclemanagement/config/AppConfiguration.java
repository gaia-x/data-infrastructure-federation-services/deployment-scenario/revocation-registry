package eu.gaiax.vc.lifecyclemanagement.config;

import id.walt.signatory.revocation.StatusListEntryFactory;
import id.walt.signatory.revocation.statuslist2021.StatusList2021EntryClientService;
import id.walt.signatory.revocation.statuslist2021.StatusListCredentialStorageService;
import id.walt.signatory.revocation.statuslist2021.StatusListIndexService;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.modelmapper.ModelMapper;

import java.util.List;

@Configuration
public class AppConfiguration implements WebMvcConfigurer {

    @Bean
    public ModelMapper modelMapper(){
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        return modelMapper;
    }

    @Bean
    public StatusListEntryFactory getStatusListEntryFactory(){
        return new StatusListEntryFactory(StatusListIndexService.Companion.getService(),
                StatusListCredentialStorageService.Companion.getService());
    }

    @Bean
    public StatusList2021EntryClientService getStatusList2021EntryClientService(){
        return new StatusList2021EntryClientService();
    }

    @Bean
    public RevocationPurposeConverter revocationPurposeConverter(){
        return new RevocationPurposeConverter();
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(revocationPurposeConverter());
    }
}

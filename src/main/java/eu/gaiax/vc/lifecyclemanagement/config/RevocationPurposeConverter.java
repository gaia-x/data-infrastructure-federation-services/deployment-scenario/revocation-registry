package eu.gaiax.vc.lifecyclemanagement.config;

import eu.gaiax.vc.lifecyclemanagement.enums.RevocationPurpose;
import org.springframework.core.convert.converter.Converter;

public class RevocationPurposeConverter implements Converter<String, RevocationPurpose> {

    @Override
    public RevocationPurpose convert(String source) {
        return RevocationPurpose.valueOf(source.toUpperCase());
    }
}
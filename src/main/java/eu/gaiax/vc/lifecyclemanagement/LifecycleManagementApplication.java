package eu.gaiax.vc.lifecyclemanagement;

import eu.gaiax.vc.lifecyclemanagement.service.RevocationService;
import id.walt.servicematrix.ServiceMatrix;
import id.walt.signatory.revocation.StatusListEntryFactoryParameter;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "Verifiable Credential Lifecycle Management",
                description = "Verifiable Credential Lifecycle Management"
        )
)
public class LifecycleManagementApplication implements CommandLineRunner {

    @Autowired
    private RevocationService revocationService;

    public static void main(String[] args) {
        SpringApplication.run(LifecycleManagementApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        new ServiceMatrix("service-matrix.properties");
    }
}

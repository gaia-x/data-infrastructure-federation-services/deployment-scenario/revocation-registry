package eu.gaiax.vc.lifecyclemanagement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TopicSubscriptionRequest {
    private String topic;
    private List<String> tokens;
}

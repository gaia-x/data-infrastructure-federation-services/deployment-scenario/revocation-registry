package eu.gaiax.vc.lifecyclemanagement.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RevocationResponseModel {
    private boolean isRevoked;
    private boolean isSuspended;
}
